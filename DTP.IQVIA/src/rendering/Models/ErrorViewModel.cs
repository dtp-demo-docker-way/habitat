namespace DTP.IQVIA.Models
{
    public class ErrorViewModel
    {
        public bool IsInvalidRequest { get; set; }
    }
}
